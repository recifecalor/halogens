{ name = "halogen-project"
, dependencies =
  [ "aff"
  , "affjax"
  , "argonaut-core"
  , "arrays"
  , "console"
  , "effect"
  , "either"
  , "foreign"
  , "halogen"
  , "halogen-subscriptions"
  , "halogen-vdom"
  , "http-methods"
  , "integers"
  , "lists"
  , "maybe"
  , "nonempty"
  , "prelude"
  , "psci-support"
  , "simple-json"
  , "web-dom"
  , "web-events"
  , "web-html"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
