module Utils 
  (
    getData
  , notReady
  ) where

import Data.NonEmpty
import Prelude

import Affjax as AX
import Affjax.ResponseFormat as AXRF
import Data.Argonaut.Core (stringify)
import Data.Array (cons)
import Data.Either (Either(..))
import Data.HTTP.Method as HTTPM
import Data.List (intercalate)
import Data.List.Types (List(..), NonEmptyList(..))
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Foreign (MultipleErrors, ForeignError(..))
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Halogen.VDom.Types as HVTypes
import Simple.JSON (readJSON)
import Web.HTML.Common as HTMLCom

getData :: forall st action output m. MonadAff m => String -> H.HalogenM st action () output m String
getData url = H.liftAff do 
      result <- AX.request (AX.defaultRequest { url = url, method = Left HTTPM.GET, responseFormat = AXRF.json })
      case result of
        Left err -> pure $ AX.printError err 
        Right response -> pure $  stringify response.body

notReady :: MultipleErrors 
notReady = NonEmptyList (ForeignError "not ready" :| Nil)
