module DataTypes where

import Affjax.RequestBody (RequestBody(..))
import Data.Either (Either)
import Data.Maybe (Maybe)
import Foreign (MultipleErrors)


data Action = Initialize  

type Pub 
  = { authors :: Array String 
    , title :: String 
    , cite :: String 
    , abstract :: Maybe String 
    , eprint :: String
    , journal :: Maybe String
    , comment :: Maybe String 
    }

type Bibitem =
  { author :: String 
  , title :: String 
  , eprint :: Maybe String 
  , journal :: Maybe String
  , year :: Maybe String 
  , volume :: Maybe String 
  , pages :: Maybe String 
  , doi :: Maybe String
  }

type Pubs = {
  googleScholarCitations :: String
  , author :: String
  , researcherID :: String
  , all :: Array Pub
  , top10 :: Array Pub 
  }

type Students
  = { phd :: { done :: Array Student, current :: Array Student, dropped :: Array Student }
    , msc :: { done :: Array Student, current :: Array Student, dropped :: Array Student }
    }

type Student 
  = { name :: String
    , position :: String
    , when_finished :: String 
    , when_started :: Maybe String 
    , notes :: Maybe String
    }

type Professional 
  = { fullName :: String 
    , googleScholarID :: String
    , orcid :: String 
    , googleScholarStats :: { citations :: Int 
                            , citations5 :: Int 
                            , hIndex :: { all :: Int , last5 :: Int } 
                            , i10 :: { all :: Int, last5 :: Int }
                            , link :: String 
                            }
    , education :: Array { institution :: String, years :: String, degree :: String }
    , employment :: Array { institution :: String, years :: String, position :: String }
    }

type Grant 
  = { number :: String
    , title :: String 
    , "type" :: String
    , myRole :: String
    , agency :: String 
    , bib :: Maybe (Array String)
    , notes :: Maybe String 
    }

type GrantX
  = { number :: String
    , title :: String 
    , "type" :: String
    , myRole :: String
    , agency :: String 
    , bib :: Maybe (Array (Either MultipleErrors Bibitem))
    , notes :: Maybe String 
    }


type Grants
  = { current :: Array Grant
    , past :: Array Grant
    , rejected :: Array Grant
    }

type GrantsX
  = { current :: Array GrantX
    , past :: Array GrantX
    , rejected :: Array GrantX
    }
