module Sumula where

import DataTypes
import Prelude
import Data.Maybe (Maybe(..), fromMaybe) 
import Affjax as AX
import Affjax.ResponseFormat as AXRF
import Data.Argonaut.Core (stringify)
import Data.Array (cons, concat)
import Data.Either (Either(..))
import Data.List (intercalate)
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Halogen.VDom.Types as HVTypes
import Simple.JSON (readJSON)
import Utils (getData, notReady, link, boldText)
import Web.HTML.Common as HTMLCom
import Web.DOM.ParentNode (QuerySelector(..))
import Web.DOM.Element (getAttribute)
import Web.HTML.HTMLElement (toElement)
import Foreign (MultipleErrors)


type Config = { root :: String }

type State
  = { publications :: Either MultipleErrors Pubs
    , students :: Either MultipleErrors Students
    , prof :: Either MultipleErrors Professional
    , grants :: Either MultipleErrors Grants
    }

main :: Effect Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  mconf <- HA.selectElement $ QuerySelector "#config"
  rootPath <- H.liftEffect case mconf of 
        Nothing -> pure Nothing
        Just elem -> getAttribute "data-rootpath" (toElement elem)
  runUI (component  { root : fromMaybe "" rootPath }) unit body

initialState :: forall input. input -> State
initialState _ = { publications : Left notReady, students: Left notReady, prof: Left notReady, grants: Left notReady}


component :: forall query input output m. MonadAff m => Config -> H.Component query input output m
component conf =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction conf, initialize = Just Initialize }
    }

renderTitle :: forall w r . State -> HH.HTML w r
renderTitle state = case state.prof of 
  Right p ->
    HH.h1 [] [ HH.text p.fullName ]
  Left e -> renderWAIT $ show e

education :: forall w r . State -> HH.HTML w r
education state = case state.prof of  
  Right p -> 
    HH.p_ 
    [ HH.h3 [] [ HH.text "Education" ] 
    , HH.table 
      []
      (cons 
       ( HH.tr [] [HH.th [] [ HH.text "Years" ], HH.th [] [ HH.text "Institution" ], HH.th [] [ HH.text "Degree granted" ]] ) 
       (map (\x -> HH.tr [] [ HH.td [] [ HH.text x.years ] , HH.td [] [ HH.text x.institution ] , HH.td [] [ HH.text x.degree ] ]) p.education))
    ]
  Left e -> renderWAIT $ show e

professionalHistory :: State -> forall w r . HH.HTML w r
professionalHistory state = case state.prof of
  Right p -> 
    HH.p_ 
    [ HH.h3 [] [ HH.text "Professional History" ] 
    , HH.table 
      []
      (cons 
       ( HH.tr [] [HH.th [] [ HH.text "Years" ], HH.th [] [ HH.text "Institution" ], HH.th [] [ HH.text "Position" ]] ) 
       (map (\x -> HH.tr [] [ HH.td [] [ HH.text x.years ] , HH.td [] [ HH.text x.institution ] , HH.td [] [ HH.text x.position ] ]) p.employment))
    ]
  Left e -> renderWAIT $ show e

renderPub :: forall w r . Pub -> HH.HTML w r
renderPub p = 
  HH.tr 
  [] 
  [ HH.td [] [HH.text p.title]
  , HH.td [] [HH.text $ intercalate ", " p.authors] 
  , HH.td [] [HH.text p.eprint]
  , HH.td [] [HH.text $ fromMaybe "" p.journal]
  ]

publicationsTop :: forall w r . State  -> HH.HTML w r 
publicationsTop state  = 
  HH.p_
  [ HH.h3 [] [ HH.text "Selected publications" ] 
  , case state.publications of 
        Right ps ->
          HH.div_ 
          [ 
           HH.table [] (map renderPub ps.top10)
          ]
        Left e -> renderWAIT $ show e
  ]

renderStudent :: forall w r . Student -> HH.HTML w r
renderStudent stud = 
  HH.tr 
  []
  [ HH.text stud.name ]

renderWAIT :: forall w r . String -> HH.HTML w r 
renderWAIT msg = HH.div_ [ HH.p_ [ HH.element (HVTypes.ElemName "font") [ HP.attr (HTMLCom.AttrName "size") "22" ] [HH.text $ "WAIT: " <> msg ] ] ]

supervisionCurrentPhD ::  forall w r . State  -> HH.HTML w r 
supervisionCurrentPhD state =
  HH.p_
  [ HH.h5 [] [ HH.text "Current PhD students" ]  
  , case state.students of 
        Right studs -> 
          HH.table [] (map renderStudent studs.phd.current)
        Left x  -> renderWAIT $ show x
  ]
supervisionCompletedPhD ::  forall w r . State  -> HH.HTML w r 
supervisionCompletedPhD state =
  HH.p_
  [ HH.h5 [] [ HH.text "Students completed PhD" ]  
  , case state.students of 
        Right studs -> 
          HH.table [] (map renderStudent studs.phd.done)
        Left x  -> renderWAIT $ show x
  ]
supervisionCurrentMSc ::  forall w r . State  -> HH.HTML w r 
supervisionCurrentMSc state =
  HH.p_
  [ HH.h5 [] [ HH.text "Current Master students" ]  
  , case state.students of 
        Right studs -> 
          HH.table [] (map renderStudent studs.msc.current)
        Left x  -> renderWAIT $ show x
  ]
supervisionCompletedMSc ::  forall w r . State  -> HH.HTML w r 
supervisionCompletedMSc state =
  HH.p_
  [ HH.h5 [] [ HH.text "Students completed MSc" ]  
  , case state.students of 
        Right studs -> 
          HH.table [] (map renderStudent studs.msc.done)
        Left x  -> renderWAIT $ show x
  ]
supervision :: forall w r . State -> Array (HH.HTML w r)
supervision state =
  [ HH.h3 [] [ HH.text "Supervision of students" ]
    , supervisionCurrentPhD state
    , supervisionCompletedPhD state 
    , supervisionCurrentMSc state 
    , supervisionCompletedMSc state
  ]

renderGrant :: forall w r . Grant -> HH.HTML w r 
renderGrant g = 
  HH.tr 
  []
  [ HH.td [] [ HH.text g.number ], HH.td [] [ HH.text g.agency ], HH.td [] [ HH.text g.title ], HH.td [] [ HH.text g.myRole ] ]

grants :: forall w r . State -> HH.HTML w r 
grants state = 
  HH.p_ 
  [ HH.h3 [] [ HH.text "Research Grants" ] 
  , case state.grants of 
        Right gs -> 
          HH.table 
          [] 
          (cons 
            (HH.tr [] [HH.th [] [ HH.text "Number" ], HH.th [] [ HH.text "Agency" ], HH.th [] [ HH.text "Title" ], HH.th [] [ HH.text "Role" ]]) 
            (map renderGrant gs.current))
        Left x  -> renderWAIT $ show x
  ]

showIndices :: forall w r . State -> HH.HTML w r
showIndices state = case state.prof of 
  Right metrics -> 
    HH.p_ 
      [ HH.h3 [] [ HH.text "Indices" ] 
      , let x = metrics.googleScholarStats.link in link x x
      , HH.table 
        []
        [ HH.tr [] [ HH.th [] [ HH.text "citations" ],  HH.td [] [ HH.text $ show metrics.googleScholarStats.citations ] ] 
        , HH.tr [] [ HH.th [] [ HH.text "citations last 5 years" ], HH.td [] [ HH.text $ show metrics.googleScholarStats.citations5 ] ] 
        , HH.tr [] [ HH.th [] [ HH.text "h-index" ], HH.td [] [ HH.text $ show metrics.googleScholarStats.hIndex.all ] ]
        , HH.tr [] [ HH.th [] [ HH.text "h-index last 5 years" ], HH.td [] [ HH.text $ show metrics.googleScholarStats.hIndex.last5 ] ]
        , HH.tr [] [ HH.th [] [ HH.text "i-index" ], HH.td [] [ HH.text $ show metrics.googleScholarStats.i10.all ] ]
        , HH.tr [] [ HH.th [] [ HH.text "i-index last 5 years" ], HH.td [] [ HH.text $ show metrics.googleScholarStats.i10.last5 ] ]
        ] 
      ]
  Left e -> renderWAIT $ show e

showResearcherID :: forall w r . State -> HH.HTML w r
showResearcherID state = case state.publications of 
  Right pubs -> 
    HH.p_ 
      [ HH.h3 [] [ HH.text "Researcher ID"]
      , link pubs.researcherID pubs.researcherID
      ]
  Left e -> renderWAIT $ show e

render :: forall cs m. State -> H.ComponentHTML Action cs m
render state =     
  HH.div_ $ concat
    [ [ renderTitle state
      , education state
      , professionalHistory state
      , publicationsTop state
      ] 
    , supervision state 
    , [ grants state
      , showIndices state
      , showResearcherID  state
      ]
    ]

handleAction :: forall output m. MonadAff m => Config -> Action -> H.HalogenM State Action () output m Unit
handleAction conf = case _ of
  Initialize -> do 
    pubs <- getData (conf.root <> "/publications")
    studs <- getData (conf.root <> "/students")
    inds <- getData (conf.root <> "/prof")
    gs <- getData (conf.root <> "/grants")
    H.modify_ \x -> x { publications = readJSON pubs 
                      , students = readJSON studs 
                      , prof = readJSON inds 
                      , grants = readJSON gs 
                      }

